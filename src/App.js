// React imports
import React, { Component } from "react";
// Importing axios for use with fetching from flickr API
import axios from "axios";

// Using Reactstrap components so I can use bootstrap components (mostly for masonry cards layout)
import {
  Navbar,
  NavbarBrand,
  Button,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  Container,
  Row,
  Col,
} from "reactstrap";

// MasonryCardList import
import MasonryCardsList from './components/MasonryCardsList';

// Function to update the state (adding more photos to the state)
// Being referenced in the onSetResult function below
const resultUpdate = result => prevState => ({
  photos: [...prevState.photos, ...result.photo],
  page: result.page,
  isLoading: false
});

// Function to do update the state from the initial/first fetch from the API
// Being referenced in the onSetResult function below
const resultCreate = result => ({
  photos: result.photo,
  page: result.page,
  isLoading: false
});


// Main App class
class App extends Component {
  constructor(props) {
    super(props);
    // Setting initial state
    this.state = { photos: [], page: null, isLoading: false };
    // Creating a ref for the search input
    this.searchTermRef = React.createRef();
  }

  // This function get's called when the search form is submitted
  onSearchSubmit = e => {
    e.preventDefault();
    
    // This is how I got the value for the search term from the input... oops :)
    const value = this.searchTermRef.current._reactInternalFiber.child.stateNode.value;

    // if empty, do nothing
    if (value === "") {
      return;
    }

    // otherwise, go get some results from the API!
    this.fetchResults(value, 0);
  };

  // This function get's called if the user scrolls to the bottom of the page.
  // Does the same as the function above, except it doesn't look for the search value
  onPaginatedSearch = e =>
    this.fetchResults(
      this.searchTermRef.current._reactInternalFiber.child.stateNode.value,
      this.state.page + 1
    );

  // The function get calls the flickr API and returns the resultset
  fetchResults = (value, page) => {
    // Set the loading state
    this.setState({ isLoading: true });

    // Actual API call
    axios({
      method: "get",
      url: "https://api.flickr.com/services/rest/",
      params: {
        method: "flickr.photos.search",
        api_key: "62e41babf670f13fe2d7b5102426b6e4",
        text: value,
        safe_search: "3",
        per_page: "20",
        page: page,
        media: "photos",
        content_type: "1",
        extras: "date_taken,owner_name,tags,url_z,url_o,url_c",
        format: "json",
        nojsoncallback: "1"
      }
    }).then(response =>
      // when a response comes back, go and update the state
      this.onSetResult(response.data.photos, page)
    );
  };

  // This updates the (initial) state accordingly
  onSetResult = (result, page) =>
    page === 0
      ? this.setState(resultCreate(result))
      : this.setState(resultUpdate(result));

  render() {
    return <div>
        <div>
          <Navbar color="dark" dark expand="lg">
            <NavbarBrand href="/">Cainthus Flickr App</NavbarBrand>
          </Navbar>
        </div>

        <Container>
          <Row>
            <Col className="text-center" lg={{ size: 10, offset: 1 }}>
            <Form type="submit" className="my-3" onSubmit={this.onSearchSubmit}>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <Button type="submit" color="primary">
                      Search!
                    </Button>
                  </InputGroupAddon>
                  <Input type="text" ref={this.searchTermRef} />
                </InputGroup>
              </Form>
            </Col>
          </Row>
        </Container>
        <Container>
          <MasonryCardsList 
            photos={this.state.photos} 
            page={this.state.page} 
            onPaginatedSearch={this.onPaginatedSearch} 
            isLoading={this.state.isLoading} />
        </Container>
      </div>;
  }
}

export default App;