import React, { Component } from "react";
import {
  Row,
  Col,
  CardColumns,
  Card,
  CardImg,
  CardTitle,
  CardBody,
  CardText, CardLink, CardFooter,
  Alert, Badge
} from "reactstrap";
import moment from 'moment';

// Main Class here
export default class MasonryCardsList extends Component {
  // Add the event listener to watch for scroll
  componentDidMount() {
    window.addEventListener("scroll", this.onScroll, false);
  }

  // Removes the event listener for scrolling when component unmounts (cleans up)
  componentWillUnmount() {
    window.removeEventListener("scroll", this.onScroll, false);
  }

  // on component update, add the listener back again
  componentDidUpdate() {
    window.addEventListener("scroll", this.onScroll, false);
  }

  onScroll = () => {
    // When the user scrolls to about 250px from the bottom of the screen...
    if (
      window.innerHeight + window.scrollY >= document.body.offsetHeight - 250 &&
      this.props.photos.length
    ) {
      // Runs this function that is actually declared in the App.js (line 68)
      this.props.onPaginatedSearch();
      // remove event listener for scroll, otherwise it's going to keep calling the API fetch  function
      window.removeEventListener("scroll", this.onScroll, false);
    }
  };

  render() {
    const { photos, isLoading } = this.props;

    return <Row>
        <CardColumns>
          {photos.map(photo => <Card key={photo.id}>
              <CardImg top width="100%" src={photo.url_z ? photo.url_z : (photo.url_c ? photo.url_c : photo.url_o)} alt={photo.title} />
              <CardBody>
                <CardTitle>{photo.title}</CardTitle>
                <CardText>Owner: {photo.ownername}</CardText>
                <CardText>Date Taken:{" "}
                  {moment(photo.datetaken).format(
                    "DD MMMM YYYY, h:mm:ss a"
                  )}</CardText>
                <CardLink target="_blank" rel="noopener noreferrer" href={`https://www.flickr.com/photos/${photo.owner}/${photo.id}`}>
                  Flickr Photo URL
                </CardLink>
              </CardBody>

              {photo.tags.indexOf(" ") > 1 ? <CardFooter>
                  <b>
                    Tags:
                  </b> {photo.tags
                    .split(" ")
                    .map(tag => (
                      <Badge className="mr-1" color="light" key={tag}>
                        {tag}
                      </Badge>
                    ))}
                </CardFooter> : <CardFooter>No Tags</CardFooter>}
            </Card>)}
        </CardColumns>
        {isLoading && <Col className="col-12 text-center">
            <Alert color="info">Loading ...</Alert>
          </Col>}
        
      </Row>;
  }
}
