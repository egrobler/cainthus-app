# Cainthus Front-end Assignment
### Developer: Ettiene Grobler
This is an interview assignment for Cainthus Front-end Developer role.
It is a simple app, that takes in a value as a search-term, and then send it off to the flickr API to get paginated results of photos and relevant meta-data. This in turn gets displayed in a masonry style on the page using Bootstrap 4 cards masonry layout.
On scroll, it will send consecutive requests to the API for the next paged resultsets.

I haven't covered all use-cases here, like what happens when the last page is reached or when no results are returned, sorry.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## To run this project
- Install dependencies first via yarn or npm

### npm
```npm install```

### yarn
```yarn install```

- Run project 
### npm
```npm run start```

### yarn
```yarn start```

This will fire up a browser in http://localhost:3000

## Dependencies
- Moment.js : Used for formatting date from the API response
- Reactstrap :  Ability to "componentise" Bootstrap 4 components and use them
- Axios : Used to fetch the results from flickr API

## App explanation / comments
I have made comments inline in the code to explain what most of the functions/code does. Hope it is sufficient.

Thanks!
Ettiene Grobler